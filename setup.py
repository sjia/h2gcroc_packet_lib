from setuptools import setup, find_packages

setup(
    name='packetlib',
    version='0.11',
    packages=find_packages(),
    author='S. Jia',
    author_email='shihai.jia@cern.ch',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
    ],
    install_requires=[
        'numpy',
    ],
)