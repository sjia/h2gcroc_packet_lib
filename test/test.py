import packetlib
import socket
import pytest

HOST   = "10.1.2.207"
UDP_IP = "10.1.2.208"
PORT   = 11000

socket_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
socket_udp.bind((HOST, PORT))

# # * Get and print the status of the device
# # * ---------------------------------------------------------------------------
# data_packet = packetlib.pack_data_req_status(0xA0, 0x00)
# # print all bytes in hex, 8 bytes per line
# print("\033[32mSending data packet:\033[0m")
# for i in range(0, len(data_packet), 8):
#     print(" ".join(f"{b:02X}" for b in data_packet[i:i+8]))
# socket_udp.sendto(data_packet, (UDP_IP, PORT))

# print("\033[32mReceived data packet:\033[0m")
# received_data, addr = socket_udp.recvfrom(1024)
# unpacked_data = packetlib.unpack_data_rpy_status(received_data)
# # print all keys and values
# max_key_length = max(len(key) for key in unpacked_data)
# for key in unpacked_data:
#     print(f"{key:<{max_key_length}} : {hex(unpacked_data[key])}")

# # * Get and print the system monitor data
# # * ---------------------------------------------------------------------------
# data_packet = packetlib.pack_data_req_sys_monitor(0xA0, 0x00)
# print("\033[32mSending data packet:\033[0m")
# for i in range(0, len(data_packet), 8):
#     print(" ".join(f"{b:02X}" for b in data_packet[i:i+8]))
# socket_udp.sendto(data_packet, (UDP_IP, PORT))

# print("\033[32mReceived data packet:\033[0m")
# received_data, addr = socket_udp.recvfrom(1024)
# unpacked_data = packetlib.unpack_data_rpy_sys_monitor(received_data)
# max_key_length = max(len(key) for key in unpacked_data)
# for key in unpacked_data:
#     print(f"{key:<{max_key_length}} : {hex(unpacked_data[key])}")

# # * Get and print the debug data
# # * ---------------------------------------------------------------------------
# data_packet = packetlib.pack_data_req_get_debug_data(0xA0, 0x00)
# print("\033[32mSending data packet:\033[0m")
# for i in range(0, len(data_packet), 8):
#     print(" ".join(f"{b:02X}" for b in data_packet[i:i+8]))
# socket_udp.sendto(data_packet, (UDP_IP, PORT))

# print("\033[32mReceived data packet:\033[0m")
# received_data, addr = socket_udp.recvfrom(1024)
# unpacked_data = packetlib.unpack_data_rpy_get_debug_data(received_data)
# max_key_length = max(len(key) for key in unpacked_data)
# for key in unpacked_data:
#     print(f"{key:<{max_key_length}} : {hex(unpacked_data[key])}")

# # * Get and print the pack counter
# # * ---------------------------------------------------------------------------
# data_packet = packetlib.pack_data_reg_get_pack_counter(0xA0, 0x00)
# print("\033[32mSending data packet:\033[0m")
# for i in range(0, len(data_packet), 8):
#     print(" ".join(f"{b:02X}" for b in data_packet[i:i+8]))
# socket_udp.sendto(data_packet, (UDP_IP, PORT))

# print("\033[32mReceived data packet:\033[0m")
# received_data, addr = socket_udp.recvfrom(1024)
# unpacked_data = packetlib.unpack_data_rpy_get_pack_counter(received_data)
# max_key_length = max(len(key) for key in unpacked_data)
# for key in unpacked_data:
#     print(f"{key:<{max_key_length}} : {hex(unpacked_data[key])}")

# # * Get and print the DAQ generator data
# # * ---------------------------------------------------------------------------
# data_packet = packetlib.pack_data_req_daq_gen_read(0xA0, 0x00)
# print("\033[32mSending data packet:\033[0m")
# for i in range(0, len(data_packet), 8):
#     print(" ".join(f"{b:02X}" for b in data_packet[i:i+8]))
# socket_udp.sendto(data_packet, (UDP_IP, PORT))

# print("\033[32mReceived data packet:\033[0m")
# received_data, addr = socket_udp.recvfrom(1024)
# unpacked_data = packetlib.unpack_data_rpy_rpy_daq_gen_read(received_data)
# max_key_length = max(len(key) for key in unpacked_data)
# for key in unpacked_data:
#     print(f"{key:<{max_key_length}} : {hex(unpacked_data[key])}")

i2c_content_digital_half_0 = [0x00,0x00,0x00,0x00,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x19,0x00,0x0a,0xcc,0xcc,0xcc,0x0c,0xcc,0xcc,0xcc,0xcc,0x0f,0x02,0x00]
assert packetlib.send_check_i2c(socket_udp, UDP_IP, PORT, asic_num=0, fpga_addr=0x00, sub_addr=packetlib.subblock_address_dict["Digital_Half_0"], reg_addr=0x00, data=i2c_content_digital_half_0, verbose=True)

assert packetlib.set_bitslip(socket_udp, UDP_IP, PORT, asic_num=0, fpga_addr=0x00, io_dly_sel=0x03, a0_io_dly_val_fclk=0x203, a0_io_dly_val_fcmd=0x400, a0_io_dly_val_tr0=0x003, a0_io_dly_val_tr1=0x003, a0_io_dly_val_tr2=0x003, a0_io_dly_val_tr3=0x003, a0_io_dly_val_dq0=0x003, a0_io_dly_val_dq1=0x003, a1_io_dly_val_fclk=0x203, a1_io_dly_val_fcmd=0x400, a1_io_dly_val_tr0=0x003, a1_io_dly_val_tr1=0x003, a1_io_dly_val_tr2=0x003, a1_io_dly_val_tr3=0x003, a1_io_dly_val_dq0=0x003, a1_io_dly_val_dq1=0x003, verbose=True)

assert packetlib.send_reset_adj(socket_udp, UDP_IP, PORT, asic_num=0, fpga_addr=0x00, sw_hard_reset_sel=0x00, sw_hard_reset=0x00, sw_soft_reset_sel=0x00, sw_soft_reset=0x00, sw_i2c_reset_sel=0x00, sw_i2c_reset=0x00, reset_pack_counter=0x00, adjustable_start=0xFF, verbose=True)

packetlib.get_system_monitor(socket_udp, UDP_IP, PORT, asic_num=0, fpga_addr=0x00, verbose=True)

packetlib.get_debug_data(socket_udp, UDP_IP, PORT, asic_num=0, fpga_addr=0x00, verbose=True)

socket_udp.close()
