# H2GCROC Packet Library

This library is a collection of classes and functions that are used to create and manipulate udp packets for communication between the KCU-H2GCROC to the PC. The library is written in Python and is based on KCU-H2GCROC manual provided by M. Czeller.

## Dependencies

The library depends on the following Python libraries:

 - `struct` (built-in)
 - `socket` (built-in)

## Installation

To install the library, clone the repository and run the following command in the root directory of the repository:

```bash
pip install .
```

## Usage

Once the library is installed, it can be **imported and used in any Python script**.

The current version (0.9) of the library supports the following packets with udp communication:

 - **System Monitor - External** (or I2C) <span style="color: green;">(tested)</span>
   - `send_check_i2c(socket, addr, port, asic_num, fpga_addr, sub_addr, reg_addr, data, verbose=True)`
 - **Get BitSlip / Set BitSlip** <span style="color: green;">(tested)</span>
   - `set_bitslip(socket, addr, port, asic_num, fpga_addr, io_dly_sel, a0_io_dly_val_fclk, a0_io_dly_val_fcmd, a0_io_dly_val_tr0, a0_io_dly_val_tr1, a0_io_dly_val_tr2, a0_io_dly_val_tr3, a0_io_dly_val_dq0, a0_io_dly_val_dq1, a1_io_dly_val_fclk, a1_io_dly_val_fcmd, a1_io_dly_val_tr0, a1_io_dly_val_tr1, a1_io_dly_val_tr2, a1_io_dly_val_tr3, a1_io_dly_val_dq0, a1_io_dly_val_dq1, verbose=True)`
 - **System Monitor - FPGA**
   - `get_system_monitor(socket, addr, port, asic_num, fpga_addr, verbose=True)`
 - **DAQ + GENERATOR Start/Stop** <span style="color: green;">(tested)</span>
   - `send_daq_gen_start_stop(socket, addr, port, asic_num, fpga_addr, daq_push, gen_start_stop, daq_start_stop, verbose=True)`
 - **Get Debug Data** <span style="color: green;">(tested)</span>
   - `get_debug_data(socket, addr, port, asic_num, fpga_addr, verbose=True)`
 - **Reset & Adjustments** <span style="color: green;">(tested)</span>
   - `send_reset_adj(socket, addr, port, asic_num, fpga_addr, sw_hard_reset_sel, sw_hard_reset, sw_soft_reset_sel, sw_soft_reset, sw_i2c_reset_sel, sw_i2c_reset, reset_pack_counter, adjustable_start, verbose=True)`

And all packet format is supported by the library for pack/unpack operations. They are defined in the `packet.py` file.

## Register settings

The library now supports explaining the register settings via I2C.

One example is the following:

```python
i2c_settings_json_path = "h2gcroc_1v4_r1.json"
reg_settings = packetlib.RegisterSettings(i2c_settings_json_path)
```

Then it can be used to get the default register settings:

```python
default_top_reg = reg_settings.get_default_reg_content('registers_top')
```

Or print the detailed information of a register:

```python
i2c_content_top = [0x0B,0x0f,0x40,0x7f,0x00,0x07,0x85,0x00]
reg_settings.explain_reg_content(i2c_content_top, 'registers_top')
```

You will get the following output:

```bash
Reg 0: 0xb
- Bit 0 RunR                 1
- Bit 1 RunL                 1
- Bit 2 TestMode             0
- Bit 3 n_counter_rst        1
- Bit 4 EdgeSel_T1           0
- Bit 5 in_inv_cmd_rx        0
- Bit 6 PreL1AOffset<0>      0
- Bit 7 PreL1AOffset<1>      0
Reg 1: 0xf
- Bit 0 EN_PLL               1
- Bit 1 DIV_PLL<0>           1
- Bit 2 DIV_PLL<1>           1
- Bit 3 FOLLOWER_PLL_EN      1
- Bit 4 sel_strobe_ext       0
- Bit 5 sel_40M_ext          0
- Bit 6 sel_error            0
- Bit 7 sel_lock             0
```

## Pedestal calibration (New in 0.9)

The key function for pedestal calibration is `set_and_measure_pedestal`. It sends the pedestal settings to the FPGA and reads the pedestal values back. Its parameters are:

- `udp_socket`: the socket object for udp communication
- `addr`: the IP address of the KCU-H2GCROC
- `port`: the port number of the KCU-H2GCROC (normally 11000)
- `fpga_addr`: the address of the FPGA (normally 0x00)
- `trim_inv_list`: the list of trim dac values **for each channel**, it should be a 1D list with 2*76 elements
- `inv_vref_list`: the list of inverted vref values **for each half**, it should be a 1D list with 4 elements
- `noinv_vref_list`: the list of non-inverted vref values **for each half**, it should be a 1D list with 4 elements
- `channel_to_ignore`: the channel number to ignore in the pedestal calibration, normally it includes the dead channels and CM(common mode) channels
- `default_chn_content`: the default I2C register content for each channel, then the function will only change the trim dac values and keep the other settings
- `default_reference_content`: the default I2C register content for the reference voltage, then the function will only change the `inv_vref` and `noinv_vref` values and keep the other settings
- `top_run_content`: this function will send the `top_run_content` to the FPGA before the pedestal measurement, normally it is to enable runL and runR bits
- `top_stop_content`: this function will send the `top_stop_content` to the FPGA after the pedestal measurement, normally it is to disable runL and runR bits
- `generator_n_cyc`: the number of measurements for each pedestal setting
- `generator_interval`: the interval between two measurements, plesae note that too small interval may cause the function to not work properly
- `_verbose`: level of verbosity, default is 1, set to 0 to disable all printings, set to 2 to enable debug printings

One example is the following:

```python
while not hammingCodePass and retry_attempt < hammingcode_max:
  retry_attempt += 1
  measurement_data = packetlib.set_and_measure_pedestal(socket_udp, h2gcroc_ip, h2gcroc_port, fpga_address, _trim_inv_list, _inv_vref_list, _noinv_vref_list, channel_not_used, chn_i2c_content, ref_i2c_content, top_reg_runLR, top_reg_offLR, gen_nr_cycle, gen_interval_value, _verbose=_meansure_verbose)
  daqH_array = measurement_data["daqh_array"]
  hammingCodePass = True
    for i in range(len(daqH_array)):
      hammingcode = [packetlib.DaqH_get_H1(daqH_array[i]), packetlib.DaqH_get_H2(daqH_array[i]), packetlib.DaqH_get_H3(daqH_array[i])]
      hammingCodePass = not any(code == 1 for code in hammingcode)
      hammingcodestr = ''.join('\033[31m1\033[0m' if code == 1 else '0' for code in hammingcode)
      if not hammingCodePass:
        print(f'Hamming Code (half {i%4}): {hammingcodestr}')
if not hammingCodePass:
  print('\033[31m' + 'Hamming Code Error after ' + str(hammingcode_max) + ' attempts' + '\033[0m')
```

In the above example, the function will try to measure the pedestal values and check the Hamming code. If the Hamming code is not correct, it will retry the measurement for `hammingcode_max` times.